
/* VERY OLD and very not so good
function getScreenWidth(removePadding){
    var viewportwidth = screen.width;
    // var viewportwidth = 0;
    // if(typeof(window.innerWidth) == 'number'){
    //     viewportwidth = window.innerWidth;
    // } else if(document.documentElement && document.documentElement.clientWidth){
    //     viewportwidth = document.documentElement.clientWidth;
    // } else if(document.body && document.body.clientWidth){
    //     viewportWidth = document.body.clientWidth;
    // };
    return viewportwidth - ((removePadding!=null && typeof(removePadding)=='number')?removePadding:0);
};
function getScreenHeight(removePadding){
    var viewportHeight = screen.height;
    // var viewportHeight = 0;
    // if (typeof(window.innerHeight) == 'number'){
    //     viewportHeight = window.innerHeight;
    // } else if(document.documentElement && document.documentElement.clientHeight){
    //     viewportHeight = document.documentElement.clientHeight;
    // } else if(document.body && document.body.clientHeight){
    //     viewportHeight = document.body.clientHeight;
    // };
    return viewportHeight - ((removePadding!=null && typeof(removePadding)=='number')?removePadding:0);
};
*/
function getScreenHeight(){ return screen.height - Math.abs(window.innerHeight - window.outerHeight); };
// function getScreenWidth(){ return screen.width; };
function getScreenWidth(removePadding){
    // var viewportwidth = screen.width;
    var viewportwidth = 0;
    if(typeof(window.innerWidth) == 'number'){
        viewportwidth = window.innerWidth;
    } else if(document.documentElement && document.documentElement.clientWidth){
        viewportwidth = document.documentElement.clientWidth;
    } else if(document.body && document.body.clientWidth){
        viewportWidth = document.body.clientWidth;
    };
    return viewportwidth - ((removePadding!=null && typeof(removePadding)=='number')?removePadding:0);
};


$.fn.center = function(positionRectifyVertical,positionRectifyHorizontal){
    if(positionRectifyVertical==null) positionRectifyVertical=0;
    if(positionRectifyHorizontal==null) positionRectifyHorizontal=0;
    this.css("position","absolute");
    this.css("top", ((getScreenHeight() - this.outerHeight()) / 2) + $(window).scrollTop() - positionRectifyVertical + "px");
    this.css("left", ((getScreenWidth() - this.outerWidth()) / 2) + $(window).scrollLeft() - positionRectifyHorizontal + "px");
    return this;
};

$.fn.reset = function(){
    $(this).each (function(){ this.reset(); });
};

$.fn.toAttention = function(intShakes, intDistance, intDuration) {
    this.each(function() {
        $(this).css("position","relative");
        for (var x=1; x<=intShakes; x++) {
            $(this).animate({left:(intDistance*-1),boxShadow: "0 0 10px #ff0000"}, (((intDuration/intShakes)/4))).animate({left:intDistance,boxShadow: "0 0 7px #008aff"},((intDuration/intShakes)/2)).animate({left:0,boxShadow: "0"},(((intDuration/intShakes)/4)),function(){  });
        };
    });
    return this;
};

$.fn.justShake = function(intShakes, intDistance, intDuration) {
    this.each(function() {
        $(this).css("position","absolute");
        var leftval = $(this).position().left;
        for (var x=1; x<=intShakes; x++) {
            $(this).animate({left:leftval+(intDistance*-1)}, (((intDuration/intShakes)/4))).animate({left:leftval+intDistance},((intDuration/intShakes)/2)).animate({left:leftval+0},(((intDuration/intShakes)/4)));
        };
    });
    return this;
};

$.fn.justGlow = function(intShakes, intDistance, intDuration) {
    this.each(function() {
        for (var x=1; x<=intShakes; x++) {
            $(this).animate({boxShadow: "0 0 "+intDistance+"px #ff0000"}, (((intDuration/intShakes)/4))).animate({boxShadow: "0 0 "+intDistance+"px #008aff"},((intDuration/intShakes)/2)).animate({boxShadow: "0"},(((intDuration/intShakes)/4)));
        };
    });
    return this;
};


/*brings text to attention by glowing it*/
$.fn.justGlowText = function(intShakes, intDistance, intDuration) {
    this.each(function() {
        for (var x=1; x<=intShakes; x++) {
            $(this).animate({textShadow: "0 0 "+intDistance+"px #ff0000"}, (((intDuration/intShakes)/4))).animate({textShadow: "0 0 "+intDistance+"px #008aff"},((intDuration/intShakes)/2)).animate({textShadow: "0"},(((intDuration/intShakes)/4)));
        };
    });
    return this;
};


$.fn.squish = function(noAnimation,callback,duration){
    if(duration==null || duration==undefined) duration = 1000;
    noAnimation = (noAnimation==null || noAnimation==undefined) ? false : true ;
    this.each(function(){
        // if($.data($(this),'squished')!=null || $.data($(this),'squished')!=undefined) $.data($(this),'squished',false);
        // if(!$.data($(this),'squished')){
            if($.data($(this),'oriWidth')!=null || $.data($(this),'oriWidth')!=undefined) $.data($(this),'oriWidth',$(this).width());
            if(noAnimation==true){
                $(this).css("width",0).hide();
            } else {
                $(this).animate({width:0,opacity:0},duration,function(){$.data($(this),'squished',true);$(this).hide();if(callback!=null || callback!=undefined) $(callback);});
            };
        // };
    });

    return this;
};
$.fn.unsquish = function(callback,duration,setWidth){
    if(duration==null || duration==undefined) duration = 1000;
    this.each(function(){
        // if($.data($(this),'squished')!=null || $.data($(this),'squished')!=undefined) $.data($(this),'squished',false);
        // if($.data($(this),'squished')){
            var oriWidth = $.data($(this),'oriWidth')!=null || $.data($(this),'oriWidth')!=undefined ? $.data($(this),'oriWidth') : (setWidth!=null || setWidth!=undefined ? setWidth : '400px');
            $(this).stop(true,true).show().animate({width:oriWidth,opacity:1},duration,function(){$.data($(this),'squished',false);if(callback!=null || callback!=undefined) $(callback);});
        // };
    });

    return this;
};


$.fn.slideOffScreen = function(direction){
    this.each(function(){
        switch(direction){
            case("left"):
                $(this).animate({left: -1*$(this).outerWidth(),opacity:0},1000);
            break;
            case("right"):
                $(this).animate({right: -1*$(this).outerWidth(),opacity:0},1000);
            break;
        };
    });
    return this;
};

$.fn.stripTags = function(){
    return this.replaceWith( this.html().replace(/<\/?[^>]+>/gi, '') );
};

$.fn.selectText = function() {
    var doc = document
        , element = this[0]
        , range, selection
    ;    
    if (doc.body.createTextRange) { //ms
        range = doc.body.createTextRange();
        range.moveToElementText(element);
        range.select();
    } else if (window.getSelection) { //all others
        selection = window.getSelection();        
        range = doc.createRange();
        range.selectNodeContents(element);
        selection.removeAllRanges();
        selection.addRange(range);
    }
}



//These below SO do not work.
/*
$.fn.persistTableHeads = function(){
    var hd = $(this).find('thead');
    if(hd.length!=0){
        var tblOffset = $(this).offset();

        hd.css({position:'absolute',width:$(this).width()});
        hd.find('th').each(function(a,b){ $(b).attr('width',$(b).width()+'px'); });

        if (($(window).scrollTop() > tblOffset.top) && ($(window).scrollTop() < tblOffset.top + $(this).height())) {
           hd.css({top:($(window).scrollTop() - (tblOffset.top + $(this).height() + hd.height()))});
        } else {
           hd.css({top:''});
        };
    };
};
function persistAllTableHeads(){

    $('table').each(function(i,v){
        var hd = $(v).find('thead');
        var tblOffset = $(v).offset();

        if (($(window).scrollTop() > tblOffset.top) && ($(window).scrollTop() < tblOffset.top + $(v).height())) {
           hd.css({position:'fixed',top:0});
        } else {
           hd.css({position:'',top:''});
       };

    });

};
*/

function addslashes(str){
    return (str + '').replace(/[\\"']/g, '\\$&').replace(/\u0000/g, '\\0');
};

function stripslashes(str){
    return (str + '').replace(/\\(.?)/g, function (s, n1) {
        switch (n1) {
        case '\\':
            return '\\';
        case '0':
            return '\u0000';
        case '':
            return '';
        default:
            return n1;
        }
    });
};

function str_replace (search, replace, subject, count) {
// *     example 1: str_replace(' ', '.', 'Kevin van Zonneveld');
// *     returns 1: 'Kevin.van.Zonneveld'
// *     example 2: str_replace(['{name}', 'l'], ['hello', 'm'], '{name}, lars');
// *     returns 2: 'hemmo, mars'
  var i = 0,
    j = 0,
    temp = '',
    repl = '',
    sl = 0,
    fl = 0,
    f = [].concat(search),
    r = [].concat(replace),
    s = subject,
    ra = Object.prototype.toString.call(r) === '[object Array]',
    sa = Object.prototype.toString.call(s) === '[object Array]';
  s = [].concat(s);
  if (count) {
    this.window[count] = 0;
  }

  for (i = 0, sl = s.length; i < sl; i++) {
    if (s[i] === '') {
      continue;
    }
    for (j = 0, fl = f.length; j < fl; j++) {
      temp = s[i] + '';
      repl = ra ? (r[j] !== undefined ? r[j] : '') : r[0];
      s[i] = (temp).split(f[j]).join(repl);
      if (count && s[i] !== temp) {
        this.window[count] += (temp.length - s[i].length) / f[j].length;
      }
    }
  }
  return sa ? s : s[0];
}

function selectTextInElement(element){
    var doc = document
        , text = doc.getElementById(element)
        , range, selection
    ;
    if (doc.body.createTextRange) { //ms
        range = doc.body.createTextRange();
        range.moveToElementText(text);
        range.select();
    } else if (window.getSelection) { //all others
        selection = window.getSelection();
        range = doc.createRange();
        range.selectNodeContents(text);
        selection.removeAllRanges();
        selection.addRange(range);
    };
};

function adddoubletick(str){
    return str.replace("'","''");
};
function stripdoubletick(str){
    return str.replace("''","'");
};

function implode(glue, pieces){
    var i = '',
        retVal = '',
        tGlue = '';
    if (arguments.length === 1){
        pieces = glue;
        glue = '';
    };
    if (typeof(pieces) === 'object') {
        if (Object.prototype.toString.call(pieces) === '[object Array]') {
            return pieces.join(glue);
        };
        for (i in pieces) {
            retVal += tGlue + pieces[i];
            tGlue = glue;
        };
        return retVal;
    };
    return pieces;
};


Object.size = function(obj) {
    var size = 0, key;
    for (key in obj) if (obj.hasOwnProperty(key)) size++;
    return size;
};

Object.hasNull = function(obj){
    for (var m in obj) if (obj[m] == null) return true;
    return false;
};

Number.padZero = function(num){
    return num<10?"0"+num:num;
};

String.prototype.isNumber = function(){return /^\d+$/.test(this);}

$.fn.forceNumeric = function(){
    return this.each(function(){
        $(this).keydown(function(e){
            var key = e.charCode || e.keyCode || 0;
            // allow backspace, tab, delete, arrows, numbers and keypad numbers ONLY
            // home, end, period, and numpad decimal
            return (key == 8 || key == 9 || key == 46 || key == 110 || key == 190 || (key >= 35 && key <= 40) || (key >= 48 && key <= 57) || (key >= 96 && key <= 105));
        });
    });
};

function nl2br (str, is_xhtml) {
  var breakTag = (is_xhtml || typeof is_xhtml === 'undefined') ? '<br ' + '/>' : '<br>';
  return (str + '').replace(/([^>\r\n]?)(\r\n|\n\r|\r|\n)/g, '$1' + breakTag + '$2');
};

$.fn.hoverDelay = function(callback,hoverDelay){
    $(this).mouseenter(function(){
        $(this).data('timeout', setTimeout(function(){
            $(callback);
//            callback(); //works too
        },(hoverDelay==null?1000:hoverDelay)));
    });
    $(this).mouseleave(function(){
        clearTimeout($(this).data('timeout'));
    });
};

$.fn.viewHider = function(){
    $(this).addClass("button");
    $(this).children().toggle(
        function(){
            $(this).hide();
        }
    );
    return $(this);
};

function in_array (needle, haystack, argStrict) {
    var key = '',
        strict = !! argStrict;

    if (strict) {
        for (key in haystack) {
            if (haystack[key] === needle) {
                return true;
            }
        }
    } else {
        for (key in haystack) {
            if (haystack[key] == needle) {
                return true;
            }
        }
    }

    return false;
}

function UrlExists(url){
    var http = new XMLHttpRequest();
    http.open('HEAD', url, false);
    http.send();
    return http.status!=404;
};

function scrollToElementOrTop(elem){
    if(elem==null || elem==undefined){
        $('html, body').animate({ scrollTop:$(document).offset().top }, 200);
    } else {
        // $(elem).parent().scrollTop = $(elem).offset().top;
        $('html, body').animate({
            scrollTop: $(elem).offset().top
        }, 200);
    };
};

function randomColour(){
    var h = '0123456789ABCDEF'.split('');
    // var c = '#';
    var c = '';
    for(var i=0;i<6;i++) c += h[Math.round(Math.random() * 15)];
    return c;
};

function getNextHighZIndex(){
    var maxZ = Math.max.apply(null,$.map($('body > *'), function(e,n){
       if($(e).css('position')=='absolute') return parseInt($(e).css('z-index'))||1 ;
       })
    );
    return maxZ;
};

$.fn.setNextHighestIndex = function(){
    $(this).css('zIndex',getNextHighZIndex());
    return $(this);
};

function generatePassword(len){
    var length = len || 10,
        charset = "abcdefghijklnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789_",
        retVal = "";
    for (var i = 0, n = charset.length; i < length; ++i) {
        retVal += charset.charAt(Math.floor(Math.random() * n));
    };
    return retVal;
};