/*
Navicat MySQL Data Transfer

Source Server         : localhost
Source Server Version : 50505
Source Host           : localhost:3306
Source Database       : zvzdb_public

Target Server Type    : MYSQL
Target Server Version : 50505
File Encoding         : 65001

Date: 2018-07-08 14:12:37
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for log_access
-- ----------------------------
DROP TABLE IF EXISTS `log_access`;
CREATE TABLE `log_access` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `datelogin` datetime DEFAULT NULL,
  `success` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=1013 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of log_access
-- ----------------------------

-- ----------------------------
-- Table structure for log_sentsms
-- ----------------------------
DROP TABLE IF EXISTS `log_sentsms`;
CREATE TABLE `log_sentsms` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `paroisse` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `from` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `to_telephones` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `message` varchar(1000) COLLATE utf8_unicode_ci DEFAULT NULL,
  `response` varchar(1000) COLLATE utf8_unicode_ci DEFAULT NULL,
  `sentby` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `sentbyid` int(11) DEFAULT NULL,
  `sentdate` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=2668 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of log_sentsms
-- ----------------------------

-- ----------------------------
-- Table structure for mobile_operators
-- ----------------------------
DROP TABLE IF EXISTS `mobile_operators`;
CREATE TABLE `mobile_operators` (
  `prefix` varchar(10) COLLATE utf8_unicode_ci DEFAULT NULL,
  `operator` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of mobile_operators
-- ----------------------------
INSERT INTO `mobile_operators` VALUES ('570', 'ORANGE');
INSERT INTO `mobile_operators` VALUES ('575', 'ORANGE');
INSERT INTO `mobile_operators` VALUES ('576', 'ORANGE');
INSERT INTO `mobile_operators` VALUES ('577', 'ORANGE');
INSERT INTO `mobile_operators` VALUES ('578', 'ORANGE');
INSERT INTO `mobile_operators` VALUES ('579', 'ORANGE');
INSERT INTO `mobile_operators` VALUES ('571', 'EMTEL');
INSERT INTO `mobile_operators` VALUES ('572', 'EMTEL');
INSERT INTO `mobile_operators` VALUES ('573', 'EMTEL');
INSERT INTO `mobile_operators` VALUES ('574', 'EMTEL');
INSERT INTO `mobile_operators` VALUES ('5831', 'ORANGE_RODRIGUES');
INSERT INTO `mobile_operators` VALUES ('5832', 'ORANGE_RODRIGUES');
INSERT INTO `mobile_operators` VALUES ('5839', 'MTML_RODRIGUES');
INSERT INTO `mobile_operators` VALUES ('5875', 'ORANGE_RODRIGUES');
INSERT INTO `mobile_operators` VALUES ('5876', 'ORANGE_RODRIGUES');
INSERT INTO `mobile_operators` VALUES ('5877', 'ORANGE_RODRIGUES');
INSERT INTO `mobile_operators` VALUES ('5878', 'ORANGE_RODRIGUES');
INSERT INTO `mobile_operators` VALUES ('5871', 'MTML_RODRIGUES');
INSERT INTO `mobile_operators` VALUES ('591', 'ORANGE');
INSERT INTO `mobile_operators` VALUES ('592', 'ORANGE');
INSERT INTO `mobile_operators` VALUES ('594', 'ORANGE');
INSERT INTO `mobile_operators` VALUES ('593', 'EMTEL');
INSERT INTO `mobile_operators` VALUES ('597', 'EMTEL');
INSERT INTO `mobile_operators` VALUES ('598', 'EMTEL');
INSERT INTO `mobile_operators` VALUES ('595', 'MTML');
INSERT INTO `mobile_operators` VALUES ('596', 'MTML');
INSERT INTO `mobile_operators` VALUES ('529', 'MTML');
INSERT INTO `mobile_operators` VALUES ('525', 'ORANGE');
INSERT INTO `mobile_operators` VALUES ('5421', 'EMTEL');
INSERT INTO `mobile_operators` VALUES ('5422', 'EMTEL');
INSERT INTO `mobile_operators` VALUES ('5423', 'EMTEL');
INSERT INTO `mobile_operators` VALUES ('5428', 'EMTEL');
INSERT INTO `mobile_operators` VALUES ('5429', 'EMTEL');
INSERT INTO `mobile_operators` VALUES ('549', 'EMTEL');
INSERT INTO `mobile_operators` VALUES ('586', 'MTML');

-- ----------------------------
-- Table structure for paroisse
-- ----------------------------
DROP TABLE IF EXISTS `paroisse`;
CREATE TABLE `paroisse` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `PAROISSE` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `REGION` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `PIC` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `LABEL` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `DESCRIPTION` varchar(1000) COLLATE utf8_bin DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=19 DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- ----------------------------
-- Records of paroisse
-- ----------------------------
INSERT INTO `paroisse` VALUES ('1', 'ST THERESE', 'CUREPIPE', 'sttherese.jpg', '&Eacute;glise Sainte-Th&eacute;r&egrave;se de Curepipe', null);
INSERT INTO `paroisse` VALUES ('2', 'ST HELENE', 'CUREPIPE', 'sthelene.jpg', '&Eacute;glise Sainte-H&eacute;l&egrave;ne de Curepipe', null);
INSERT INTO `paroisse` VALUES ('3', 'NOTRE DAME DE LA MER', 'ALBION', 'notredamedelamer.jpg', '&Eacute;glise Notre Dame de la mer d\'Albion', null);
INSERT INTO `paroisse` VALUES ('4', 'ST COEUR DE MARIE', 'PETITE RIVIERE', 'petiteriviere.jpg', '&Eacute;glise Saint-Coeur-de Marie de Petite Rivi&egrave;re', null);
INSERT INTO `paroisse` VALUES ('5', 'ST SAUVEUR', 'BAMBOUS', 'bambous.jpg', '&Eacute;glise Saint-Sauveur de Bambous', null);
INSERT INTO `paroisse` VALUES ('6', 'Sainte Marie Madeleine', 'PTE AUX SABLES', 'stmariemadeleine.jpg', 'Sainte Marie Madeleine', null);
INSERT INTO `paroisse` VALUES ('7', 'MONT MARTE', 'ROSE HILL', 'montmarte.jpg', 'La Chapelle de Montmartre', null);
INSERT INTO `paroisse` VALUES ('9', 'Saint Matthieu', 'POINTE AUX SABLES', 'stmatthieu.jpg', 'Eglise Saint Matthieu', null);
INSERT INTO `paroisse` VALUES ('10', 'BAIE DU TOMBEAU', 'BAIE DU TOMBEAU', 'stmalo.jpg', '&Eacute;glise Saint Malo', null);
INSERT INTO `paroisse` VALUES ('11', 'CELLULE COM', 'GLOBALE', 'icjm.jpg', 'Institut Cardinal Jean Marg&eacute;ot', null);
INSERT INTO `paroisse` VALUES ('12', 'SOUILLAC', 'SOUILLAC', 'Souillac-St-Jacques-Church-1855.jpg', '&Eacute;glise Sainte-Th&eacute;r&egrave;se St Jacques', null);
INSERT INTO `paroisse` VALUES ('13', 'ST THERESE (GI - INVITE)', 'CUREPIPE', 'invite.jpg', '&Eacute;glise Sainte-Th&eacute;r&egrave;se de Curepipe', null);
INSERT INTO `paroisse` VALUES ('14', 'ST THERESE 2', 'CUREPIPE', 'sttherese.jpg', '&Eacute;glise Sainte-Th&eacute;r&egrave;se de Curepipe', null);
INSERT INTO `paroisse` VALUES ('15', 'ZVZ SAINT JEAN', 'QUATRES BORNES', 'stjean.jpg', '&Eacute;glise St Jean', null);
INSERT INTO `paroisse` VALUES ('16', 'ZVZ SACRE CŒUR', 'BEAU BASSIN', 'sacrecoeur.jpg', '&Eacute;glise Sacre Coeur', null);
INSERT INTO `paroisse` VALUES ('17', 'ZVZ SAINT PATRICK', 'SAINT PATRICK', 'saintpierre.jpg', '&Eacute;glise Saint Patrick', 'Saint Pierre ou Saint Patrick');
INSERT INTO `paroisse` VALUES ('18', 'ZVZ SACRE COEUR 2011', 'BEAU BASSIN', 'sacrecoeur.jpg', '&Eacute;glise Sacre Coeur', 'Parcours \"ZEZI VRE ZOM\" à Beau-Bassin du 14 Septembre au 23 Novembre 2011');

-- ----------------------------
-- Table structure for users
-- ----------------------------
DROP TABLE IF EXISTS `users`;
CREATE TABLE `users` (
  `id` bigint(11) NOT NULL AUTO_INCREMENT,
  `idnum` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `Nom` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `Prenom` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `alias` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `DOB` date DEFAULT NULL,
  `Profession` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `ETUDE` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `STATUT` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `ADRESSE1` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `ADRESSE2` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `TELHOME` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `TELWORK` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `TELMOB` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `TELMOBTYPE` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `TELMOB2` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `TELMOB2TYPE` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `EMAIL` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `WEBSITE_PERSO` varchar(500) COLLATE utf8_bin DEFAULT NULL,
  `WEBSITE_PRO` varchar(500) COLLATE utf8_bin DEFAULT NULL,
  `FACEBOOK` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `LINKEDLN` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `Login` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `MDP` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `MDPA` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `ADMIN` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `USERTYPE` varchar(20) COLLATE utf8_bin DEFAULT NULL,
  `PAROISSE` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `GROUPE` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `REFIMPORT` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `SENDCREDENTIALS` int(11) DEFAULT '0',
  `CUVE` varchar(255) COLLATE utf8_bin DEFAULT NULL COMMENT '2011,2012,TERRE SAINTE 2014, etc..',
  `DATEMODIFIED` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `SMS_NEXMO_EMAIL_LOGIN` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `SMS_NEXMO_MOBILE_NUMBER` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `SMS_NEXMO_KEY` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `SMS_NEXMO_SECRET` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `SMS_NEXMO_GRANTED` int(11) NOT NULL DEFAULT '0' COMMENT '0 or 1',
  `SENDLOGIN_EMAIL` int(11) NOT NULL DEFAULT '0',
  `SENDLOGIN_SMS` int(11) NOT NULL DEFAULT '0',
  `SENDLOGIN_EMAIL_ERROR` int(11) NOT NULL DEFAULT '0',
  `SENDLOGIN_SMS_ERROR` int(11) NOT NULL DEFAULT '0',
  `DATELASTACCESSED` datetime DEFAULT NULL,
  `IPLASTACCESSED` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2268 DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- ----------------------------
-- Table structure for users_demo
-- ----------------------------
DROP TABLE IF EXISTS `users_demo`;
CREATE TABLE `users_demo` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `idnum` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `Nom` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `Prenom` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `alias` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `DOB` date DEFAULT NULL,
  `Profession` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `ETUDE` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `STATUT` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `ADRESSE1` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `ADRESSE2` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `TELHOME` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `TELWORK` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `TELMOB` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `TELMOBTYPE` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `TELMOB2` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `TELMOB2TYPE` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `EMAIL` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `WEBSITE_PERSO` varchar(500) COLLATE utf8_bin DEFAULT NULL,
  `WEBSITE_PRO` varchar(500) COLLATE utf8_bin DEFAULT NULL,
  `FACEBOOK` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `LINKEDLN` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `Login` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `MDP` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `MDPA` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `ADMIN` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `USERTYPE` varchar(20) COLLATE utf8_bin DEFAULT NULL,
  `PAROISSE` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `GROUPE` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `REFIMPORT` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=203 DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- ----------------------------
-- Records of users_demo
-- ----------------------------
