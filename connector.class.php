<?php @session_start();



    include_once("core/data.class.php");

    include_once("core/dataRun.class.php");

    include_once("core/extraCores.class.php");



    class connector{



        PRIVATE STATIC $CONNECTOR;

        PRIVATE STATIC $DBCONNECTION;

        PRIVATE STATIC $QUERYRUNNER;

        PRIVATE STATIC $BOXSTREAM;



        PUBLIC STATIC $NON_EDITABLE_FIELDS = array('MDPA','id','idnum','DOB','Nom','Prenom');

        PUBLIC STATIC $NON_VISIBLE_FIELDS = array('MDPA','id','idnum');



        public function __construct(){

            self::$CONNECTOR = new dbOpen();

            if(self::is_localhost()){

                self::$DBCONNECTION = self::$CONNECTOR->getConnectionTo('local');

            } else {

                self::$DBCONNECTION = self::$CONNECTOR->getConnectionTo('main');

            };



            self::$QUERYRUNNER = new dataRun(self::$DBCONNECTION,dataRun::$AVAILABLEDATABASETYPES[0]);

        }



        public static function getEmployeeList($listInvalidToo=true){

            return self::$QUERYRUNNER->runQueryForData("SELECT * FROM users WHERE PAROISSE='{$_SESSION['PAROISSE']}' ORDER BY Prenom");

        }



        public static function checkLogin($uname,$passwd){

            $ret = self::$QUERYRUNNER->checkIfExists("SELECT id FROM users WHERE Login='$uname' AND MDP='$passwd'") ? 'ok' : 'Votre identifiant ou mot de passe est incorrect.';

            $_SESSION = self::getUserDetails($uname);

            return $ret;

        }



        public static function getUserDetails($uname){

            return self::$QUERYRUNNER->runQueryForData("SELECT * FROM users WHERE Login='$uname'");

        }



        public static function getPariosseDetails($name){

            return self::$QUERYRUNNER->runQueryForData("SELECT * FROM paroisse WHERE PAROISSE='$name'");

        }



        public static function getProfessionList(){

             return self::$QUERYRUNNER->runQueryForData("SELECT DISTINCT Profession FROM users WHERE (Profession<>'' AND Profession IS NOT NULL AND Profession <> 'null')");

        }



        public static function getJobList(){

            return self::$QUERYRUNNER->runQueryForData("SELECT DISTINCT Profession FROM users ORDER BY Profession ASC");

        }



        public static function getRegionList(){

            return self::$QUERYRUNNER->runQueryForData("SELECT DISTINCT ADRESSE2 region FROM users ORDER BY ADRESSE2 ASC");

        }



        public static function getParoisseList(){

//            return self::$QUERYRUNNER->runQueryForData("SELECT DISTINCT PAROISSE FROM users ORDER BY PAROISSE ASC");

            return self::$QUERYRUNNER->runQueryForData("SELECT * FROM paroisse ORDER BY PAROISSE ASC");

        }



        public static function getMonthFromNum($monthNum){

            $mnths = array(1=>'Janvier','Février','Mars','Avril','Mai','Juin','Juillet','Août','Septembre','Octobre','Novembre','Décembre');

            return $mnths[$monthNum];

        }



        public static function is_localhost(){ return in_array($_SERVER['REMOTE_ADDR'], array( 'asteklt92','localhost','127.0.0.1', '::1' )) ? true : false; }



        public static function doSearch($data){

            //searchJobType : Profession

            //searchRegion : ADRESSE2

            //searchParoisse : PAROISSE

            $srchJOB = !isset($data['searchJobType']) ? null : (count($data['searchJobType'])<=1 ? (count($data['searchJobType'])<=0 ? null : $data['searchJobType'][0]) : implode("', '", $data['searchJobType']));



            $srchREGION = !isset($data['searchRegion']) ? null : (count($data['searchRegion'])<=1 ? (count($data['searchRegion'])<=0 ? null : $data['searchRegion'][0]) : implode("', '", $data['searchRegion']));



            $srchPAROISSE = !isset($data['searchParoisse']) ? null : (count($data['searchParoisse'])<=1 ? (count($data['searchParoisse'])<=0 ? null : $data['searchParoisse'][0]) : implode("', '", $data['searchParoisse']));



            $qry = 'SELECT * FROM users WHERE ';

            if($srchJOB!=null && isset($data['searchJobType'])){

                $qry .= "Profession IN ('$srchJOB')";

                if($srchREGION!=null && isset($data['searchRegion'])) $qry .= " AND ";

            };



            if($srchREGION!=null && isset($data['searchRegion'])){

                $qry .= "ADRESSE2 IN ('$srchREGION')";

                if($srchPAROISSE!=null && isset($data['searchParoisse'])) $qry .= " AND ";

            };



            if($srchPAROISSE!=null && isset($data['searchParoisse'])) $qry .= "PAROISSE IN ('$srchPAROISSE')";



            $qry .= " ORDER BY Prenom, Nom ASC";



            return self::$QUERYRUNNER->runQueryForData($qry);

        }



        public static function save_profile($data){

            $errObj = array();

            $err = false;



            if(!is_null($data)){



                $qry = 'UPDATE USERS SET ';



                if(isset($data['nomVar']) && trim($data['nomVar'])!=''){ $qry .= 'Nom="'.$data['nomVar'].'", '; } else {

                    $err = true;

                    array_push($errObj,array('target'=>'nomVar','msg'=>'Nom non-saisi.')); };



                if(isset($data['prenomVar']) && trim($data['prenomVar'])!=''){ $qry .= 'Prenom="'.$data['prenomVar'].'", '; } else {

                    $err = true;

                    array_push($errObj,array('target'=>'prenomVar','msg'=>'Pr&eacute;nom non-saisi.')); };



                if(isset($data['aliasVar'])){ $qry .= 'alias="'.$data['aliasVar'].'", '; };



                if(isset($data['dobVar']) && trim($data['dobVar'])!=''){ $qry .= 'DOB="'.$data['dobVar'].'", '; } else {

                    $err = true;

                    array_push($errObj,array('target'=>'dobVar','msg'=>'Date de naissance non-saisi.')); };



                if(isset($data['professionVar']) && trim($data['professionVar'])!=''){ $qry .= 'Profession="'.$data['professionVar'].'", '; } else {

                    $err = true;

                    array_push($errObj,array('target'=>'professionVar','msg'=>'Prof&eacute;ssion non-saisi.')); };



                if(isset($data['etudeVar'])){ $qry .= 'ETUDE="'.$data['etudeVar'].'", '; };



                if(isset($data['statutVar'])){ $qry .= 'STATUT="'.$data['statutVar'].'", '; };



                if(isset($data['adresse1Var']) && trim($data['adresse1Var'])!=''){ $qry .= 'ADRESSE1="'.$data['adresse1Var'].'", '; } else {

                    $err = true;

                    array_push($errObj,array('target'=>'adresse1Var','msg'=>'Adresse 1 non-saisi.')); };



                if(isset($data['adresse2Var']) && trim($data['adresse2Var'])!=''){ $qry .= 'ADRESSE2="'.$data['adresse2Var'].'", '; } else {

                    $err = true;

                    array_push($errObj,array('target'=>'adresse2Var','msg'=>'Adresse 2 non-saisi.')); };



                if(isset($data['telhomeVar'])){ $qry .= 'TELHOME="'.$data['telhomeVar'].'", '; };



                if(isset($data['telworkVar'])){ $qry .= 'TELWORK="'.$data['telworkVar'].'", '; };



                if(isset($data['telmobVar']) && trim($data['telmobVar'])!=''){ $qry .= 'TELMOB="'.$data['telmobVar'].'", '; } else {

                    $err = true;

                    array_push($errObj,array('target'=>'telmobVar','msg'=>'Num&eacute;ro t&eacute;l&eacute;phone mobile non-saisi.')); };



                if(isset($data['emailVar'])){ $qry .= 'EMAIL="'.$data['emailVar'].'", '; };



                if(isset($data['loginVar']) && trim($data['loginVar'])!=''){ $qry .= 'Login="'.$data['loginVar'].'", '; } else {

                    $err = true;

                    array_push($errObj,array('target'=>'loginVar','msg'=>'Identifiant non-saisi.')); };



                if(isset($data['mdpVar']) && trim($data['mdpVar'])!=''){ $qry .= 'MDP="'.$data['mdpVar'].'", '; } else {

                    $err = true;

                    array_push($errObj,array('target'=>'mdpVar','msg'=>'MDP non-saisi.')); };



                $qry .= 'idnum=idnum WHERE id='.$data['idVar'];



                $ret = $err ? json_encode($errObj) : self::$QUERYRUNNER->runQuerySimple($qry);

            } else {

                array_push($errObj,array('target'=>'monProfilForm','msg'=>'Data sent is null!'));

                $ret = json_encode($errObj);

            };



            return $ret;

        }



        public static function getUserDetailsByParoisse($paroisse){

            return self::$QUERYRUNNER->runQueryForData("SELECT * FROM users WHERE PAROISSE='$paroisse'");

        }



    }

?>

