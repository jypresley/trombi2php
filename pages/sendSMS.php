<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<html>
<head>
<meta http-equiv="Expires" content="Fri, Jan 01 1900 00:00:00 GMT">
<meta http-equiv="Pragma" content="no-cache">
<meta http-equiv="Cache-Control" content="no-cache">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<meta http-equiv="Lang" content="en">
<meta name="author" content="">
<meta http-equiv="Reply-to" content="@.com">
<meta name="generator" content="PhpED 5.6">
<meta name="description" content="">
<meta name="keywords" content="">
<meta name="creation-date" content="11/11/2008">
<meta name="revisit-after" content="15 days">
<title>SmS</title>
<link rel="stylesheet" type="text/css" href="my.css">
<style>
#btnSend {
    border: 1px solid #B7DCFF;
    border-radius: 3px;
    color: #667788;
    display: inline;
    padding: 5px;
    transition: all 0.3s ease 0s;
    width: 20%;
}
#selTo {
    border: 1px solid #B7DCFF;
    border-radius: 3px;
    color: #999999;
    display: inline;
    padding: 5px;
    transition: all 0.3s ease 0s;
    width: 2%;
    height:2%;
}
.mult,#fwd,#mobTr{

    display:none;
}

#fwd,#back{

    display:none;
    float:right;
}

.inputRadio {
    border: 1px solid #B7DCFF;
    border-radius: 3px;
    color: #999999;
    display: inline;
    padding: 5px;
    transition: all 0.3s ease 0s;
    width: 1%;
}

#btnSend:hover {
background-color: #99FF99;
}
</style>
</head>
<body>
<?php
    include("../connector.class.php");
    $con = new connector();
    $paroisseLists = $con->getParoisseList();
?>
<form action="sendSMS.php" method="get">
<table border="0">
<tr>
<td width="20%">Paroisse</td>
<td colspan="2">
<select name="selParoisse" id="selParoisse">
<option value=''>-------</option>
<?php foreach($paroisseLists as $paroisseList) :?>
<option value='<?php echo $paroisseList['PAROISSE'];?>'><?php echo $paroisseList['PAROISSE'];?></option>
<?php endforeach;?>
</select>
</td>
</tr>
<tr id="mobTr"><td>Num&eacute;ro Portable</td>
    <td>
    <select multiple id="selectNumbersPerParoisse" class="mult" style="width: 300px;"></select>
    <div id="fwd"><img src='assets/imgs/forward.png' width="25px;"></div>
    <div id="back"><img src='assets/imgs/backward.png' width="25px;"></div>
    </td>
    <td><select multiple id="selectedNumbersPerParoisse" name="selectedNumbersPerParoisse" class="mult" style="width: 300px;"></select></td>
</tr>
<tr>
    <td>Messages</td><td colspan="2"><textarea name="msgs" id="msgs" rows="5" cols="38" maxlength="150"></textarea><td>
</tr>
<tr>
    <td>Types</td><td><input type="radio" class="inputRadio" name="typeUser" value="ADMIN">ADMIN<input class="inputRadio" type="radio" name="typeUser" value="ANIMATEUR">ANIMATEUR<input class="inputRadio" type="radio" name="typeUser" value="NORMAL">NORMAL<input class="inputRadio" type="radio" name="typeUser" value="AUCUN" checked="checked">AUCUN
    <td>
</tr>
<tr>
    <td width="20%">&nbsp;</td><td><input type="button" name="btnSend" id="btnSend" value="SEND"></td>
</tr>
</table>
</form>
</body>
</html>
<script>
$(function() {
        $("#selParoisse").change(function() {
            var selParoisse = $(this).val();

            $.ajax({
                type: "GET",
                url: "pages/getUsersByParoisse.php",
                data: { selParoisse: selParoisse },
                dataType: "json"
            }).done(function(reply) {
                $("#selectNumbersPerParoisse").show();
                $("#selectedNumbersPerParoisse").show();
                $("#fwd").show();
                $("#back").show();
                $("#mobTr").show();
                $("#selectedNumbersPerParoisse").show();
                $("#selectNumbersPerParoisse").find("option").remove();
                $("#selectedNumbersPerParoisse").find("option").remove();
                // Loop through JSON response
                $.each(reply, function(key, value) {
                    $('#selectNumbersPerParoisse').append($('<option>', { value: value.TELMOB }).text(value.Nom+' '+value.Prenom+' - '+value.TELMOB+'('+value.TELMOBTYPE+')'));
                });
            });

        });
    });


$('#btnSend').click(function(){
var mobs = $('select#selectedNumbersPerParoisse').val();
var msgs = $('#msgs').val();

if(mobs==null || mobs===false || msgs=="")
{
    alert("Champs requis");
    return false;
}
else
{
    $.post('pages/sendBatchSms.php', {'mobs':mobs,'msgs':msgs},
            function(data)
            {
                if(data)
                {
                    alert(data);
                }
                else
                {
                   alert("Votre messages n'a �t� envoy�. Veuillez r�essayez.");
                }
            }
        );
}


});


$("input:radio[name=typeUser]").click(function() {
    var selUserType = $(this).val();
    var selParoisse = $("#selParoisse").val();
    $.ajax({
        type: "GET",
        url: "pages/getUsersByParoisseUserType.php",
        data: { selParoisse: selParoisse,selUserType: selUserType },
        dataType: "json"
    }).done(function(reply) {
        $("#selectNumbersPerParoisse").show();
        $("#selectedNumbersPerParoisse").show();
        $("#selectedNumbersPerParoisse").find("option").remove();
        // Loop through JSON response
        $.each(reply, function(key, value) {
            $('#selectedNumbersPerParoisse').append($('<option>', { value: value.TELMOB }).text(value.Nom+' '+value.Prenom+' - '+value.TELMOB+'('+value.TELMOBTYPE+')'));
        });
    });

    });

$('#fwd').click(function(){
return !$('#selectNumbersPerParoisse option:selected').remove().appendTo('#selectedNumbersPerParoisse');
});
$('#back').click(function(){
return !$('#selectedNumbersPerParoisse option:selected').remove().appendTo('#selectNumbersPerParoisse');
});


</script>

<br><br><br>