<?php
include("../connector.class.php");
    $con = new connector();

    $jobsList = $con->getJobList();
    if(isset($jobsList['Profession'])) $jobsList = array($jobsList);
//    echo '<pre>';print_r($jobsList);echo '</pre>';

    $regionList = $con->getRegionList();
    if(isset($regionList['region'])) $regionList = array($regionList);
//    echo '<pre>';print_r($regionList);echo '</pre>';

    $paroisseList = $con->getParoisseList();
    if(isset($paroisseList['PAROISSE'])) $paroisseList = array($paroisseList);
//    echo '<pre>';print_r($paroisseList);echo '</pre>';

//render results if adequate
if(isset($_POST['resultsData'])){
    $hasresults = true;
    $results = json_decode($_REQUEST['resultsData'],true);
    if(isset($results['Login'])) $results = array($results);

//    echo '<pre>';print_r($results);echo '</pre>';
} else {
    $hasresults = false;
    $results = array();
};
?>

<script>
    window.setTimeout(function(){
        $('.makeChosen').chosen();
    },100);
</script>

<div align="center">
    <form id='searchFiltersForm'>
    Rechercher un
        <!--profession-->&nbsp;
        <span class='imgrectify10px'>
            <select multiple="multiple" style="width:200px;" data-placeholder="ami par profession(s)" onchange="" name='searchJobType[]' id='searchJobType[]' class="makeChosen">
                <option></option>
                <?php
                    foreach($jobsList as $job){
                        echo "<option value='{$job['Profession']}'>{$job['Profession']}</option>";
                    };
                ?>
            </select>
        </span>
     &nbsp;dans la r&eacute;gion de
        <!--region-->&nbsp;
        <span class='imgrectify10px'>
            <select multiple="multiple" style="width:200px;" data-placeholder="choisir r&eacute;gion(s)..." onchange="" name='searchRegion[]' id='searchRegion[]' class="makeChosen">
                <option></option>
                <?php
                    foreach($regionList as $region){
                        echo "<option value='{$region['region']}'>{$region['region']}</option>";
                    };
                ?>
            </select>
        </span>
     &nbsp;ou dans la paroisse de
        <!--paroisse-->&nbsp;
        <span class='imgrectify10px'>
            <select multiple="multiple" style="width:200px;" data-placeholder="choisir paroisse(s)..." onchange="" name='searchParoisse[]' id='searchParoisse[]' class="makeChosen">
                <option></option>
                <?php
                    foreach($paroisseList as $paroisse){
                        echo "<option value='{$paroisse['PAROISSE']}'>{$paroisse['PAROISSE']}</option>";
                    };
                ?>
            </select>
        </span>.

        <div onclick="doSearch('#searchFiltersForm');" align="center" style="width:200px;padding:5px;margin:3px;" class="button">Rechercher amis<span style="float:right;"><img src='assets/imgs/switch_user.png' /></span></div>
     </form>
 </div>

 <hr />

 <div style="text-align:center;padding:10px;" id='searchAmiResults'>
    <span style="display:none;" id='searchAmiResultsLoading'>
        Veuillez patienter lors de la recherche...
        <br />
        <img class='imgrectify5px' src='assets/imgs/small_loading3.gif' style="border-radius:4px;border:2px solid #fff;" />
    </span>
    <?php
        if($hasresults){
            if(count($results)<=0){ echo 'Aucun r&eacute;sultat retrouv&eacute;'; } else {

                echo "<table style='width:100%;' class='genericTable tableSorter'><thead>";
                echo "<tr>
                    <th>Nom</th>
                    <th>Prof&eacute;ssion</th>
                    <th>T&eacute;l&eacute;phone</th>
                    <th>email</th>
                    <th>Paroisse</th>
                </tr>";
                echo "</thead><tbody>";

                foreach($results as $uid=>$res){
                    echo "
                        <tr>
                            <td>{$res['Prenom']} {$res['Nom']} ".($res['alias']!='' ? '('.$res['alias'].')' : '')."</td>
                            <td>".($res['Profession']=='' ? 'Aucune prof&eacute;ssion.' : $res['Profession'])."</td>
                            <td>".($res['TELMOB']=='' ? ($res['TELWORK']=='' ? ($res['TELHOME']=='' ? '(Aucun num&eacute;ro)' : $res['TELHOME']) : $res['TELWORK']) : $res['TELMOB'])."</td>
                            <td>{$res['EMAIL']}</td>
                            <td>{$res['PAROISSE']}</td>
                        </tr>
                    ";
                };

                echo "</tbody></table>";

            };

        } else {
            echo "Veuillez &eacute;ffectuer une recherche.";
        };
    ?>
 </div>