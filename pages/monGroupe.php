﻿<?php
    include("../connector.class.php");
    $con = new connector();

    $empList = $con->getEmployeeList();
//    echo '<pre>';print_r($empList);echo '</pre>';
?>

<style>
    .stampBG:hover{
        box-shadow:0 0 10px 0 #444;
    }
    .stampBG{
        border: 5px solid #fff;
        box-shadow:0 0 10px -3px #222;
        border-radius:4px;
        padding:0;
        margin:10px;
        display: inline-block;
        width:22%;
        text-align: left;
        vertical-align: middle;
    }
    .stamped{
/*        border-radius:4px;*/
        display: inline-block;
        float:left;
        background:#fff;
/*        margin:-1px;*/
        padding:5px;
        width:98%;
    }
    .stampedGenderM{
        border-color:#87c5f1;
        background-color:#87c5f1;
    }
    .stampedGenderF{
        border-color:#f28cc6;
        background-color:#f28cc6;
    }
    .notHere{
        opacity:0.4;
    }
    .notHere:hover{
        opacity:0.9;
    }
    .userData{
        display:inline-block;
        max-width:56%;
        margin:4px;
        vertical-align: top;
        margin-top:0;
        font-size:14px;
        margin-bottom:0;
        float:left;
    }
    .userDataContacts{
        padding:0 4px;
        padding-left:0;
        color:#fff;
        border-radius:2px;
        display:inline-block;
        font-size:11px;
        margin:1px;
    }
    .userDataImage img{
        width:auto;
        max-height:30%;
        float:right;
    }
    .userDataImage{
        display:inline-block;
        width:30%;
        float:right;
    }
</style>

<script>

    $(document).ready(function(){
        $('.notHere').hide();
        $('#trombiUserChosen').chosen();
    });

    function scrollToTrombiUser(elem){
        var elem = elem || "#"+$('#trombiUserChosen').val();
        $('.MYBody').animate({scrollTop:$(elem).offset().top-20},'slow',function(){$(elem).toAttention(10,5,2000);});
    };

    function triggerRIPS(){
        $('.notHere').each(function(i,v){$(v).delay(i*300).slideToggle()});

    };

</script>

    <div>
        <center>
        <div>

            <div style="display: inline-block;border:1px solid #999;border-radius:3px;background:#fff;padding:4px 3px;vertical-align: middle;" align="left">
                &nbsp;<img src='assets/imgs/user_u_icon.png' class='imgrectifyup3px' />&nbsp;
                <select style="width:360px;" data-placeholder="Rechercher Ami..." onchange="scrollToTrombiUser()" id='trombiUserChosen'>
                    <option></option>
                    <?php
                        foreach($empList as $emp){
                            echo "<option value='user_{$emp['idnum']}'>{$emp['Prenom']} ".ucfirst(strtolower($emp['Nom']))." - {$emp['alias']} ( ".($emp['Profession']==''?'Aucune prof&eacute;ssion':$emp['Profession'])." )</option>";
                        };
                    ?>
                </select>
                &nbsp;
            </div>

        </div>
        </center>
        <br>
    </div>

    <div id='trombiMainContainer' align="center">
    <?php
        foreach($empList as $emp){

            if(isset($emp['DOB'])){
                /*$date1 = new DateTime($emp['DOB']);
                $date2 = new DateTime(date("Y-m-d"));
                $interval = $date1->diff($date2);
                $nYrs = $interval->y;
                $nDays = $interval->d;
                $nMnth = $interval->m;
                $employedSinceStr = $nYrs."ans";*/
                $dobbits = explode('-',$emp['DOB']);
                $employedSinceStr = $dobbits[2].' '.$con->getMonthFromNum((int)$dobbits[1]);
            } else {
                $employedSinceStr = '';
            };

            /*$userIMG = file_exists("assets/imgs/userpics/{$emp['idnum']}.jpg") ? "<img style='border-radius:3px;border:1px solid #ccc;' src='assets/imgs/userpics/{$emp['idnum']}.jpg' />" : "<a class='button' href='mailto:jypresley@hotmail.com&subject=ZVZPicture'><img style='border-radius:3px;border:1px solid #ccc;' src='assets/imgs/userpics/who.png' /></a>";
            $userFullName = $emp['Prenom'].' '.$emp['Nom'];

            echo "<div id='user_{$emp['idnum']}' class='trombi-searchable stampBG'>
            <div class='stamped'>";

            echo '<div align="top" class="userDataImage tooltipAUTO" title="<span>'.$userFullName.'<br />'.$userIMG.'</span>"</div>';

            echo "<div align='top' class='userData'>
                <b>{$userFullName} ".($emp['alias']=='' || in_array($emp['alias'],explode(" ",$userFullName))?"":"<i style='font-weight:normal;color:#999;font-size:10px;'>({$emp['alias']})</i>")."</b><br>*/

                switch($emp['USERTYPE']){
                    case("ANIMATEUR"):
                        $extraimgtoshow = "<div style='display: inline-block; margin-top: -40px; float: right; vertical-align: -40px;' Title='Animateur'><img src='assets/imgs/animatorstar.jpg' width='40' height='40' style='border-top-left-radius: 5px;' /></div>";
                    break;
                     case("PRETRE"):
                        $extraimgtoshow = "<div style='display: inline-block; margin-top: -40px; float: right; vertical-align: -40px;'><img src='assets/imgs/pretrestar.gif' width='40' height='40' style='border-top-left-radius: 5px;' /></div>";
                    break;
                     case("ADMIN"):
                        $extraimgtoshow = "<div style='display: inline-block; margin-top: -40px; float: right; vertical-align: -40px;'><img src='assets/imgs/adminstar.png' width='40' height='40' style='border-top-left-radius: 5px;' /></div>";
                    break;
                    default :
                        $extraimgtoshow = "";
                    break;
                }

            $userIMG = file_exists("../assets/imgs/userpics/{$emp['idnum']}.jpg") ? "assets/imgs/userpics/{$emp['idnum']}.jpg" : 'assets/imgs/userpics/who.png';
            $userFullName = $emp['Prenom'].' '.$emp['Nom'];

            echo "<div id='user_{$emp['idnum']}' class='trombi-searchable stampBG'>
            <div class='stamped'>";
            echo "<div align='top' class='userDataImage tooltipS' title=\"<span>{$userFullName}<br /><img src='assets/imgs/userpics/{$emp['idnum']}_bigger.jpg' /></span>\"><img style='border-radius:3px;border:1px solid #ccc;' src='".$userIMG."' />".$extraimgtoshow."<br><span style='font-size:9px'>{$emp['GROUPE']}</span></div>";
            echo "<div align='top' class='userData'>
                <b>{$userFullName} ".($emp['alias']=='' || in_array($emp['alias'],explode(" ",$userFullName))?"":"<i style='font-weight:normal;color:#999;font-size:10px;'>({$emp['alias']})</i>")."</b><br>

                <span class='userDataContacts' style='vertical-align:1px;background-color:#daf195;color:#5d6a36;border-color:#5d6a36;'><span class='tooltipW noSpace' title='Profession'><img class='imgrectify5px' src='assets/imgs/certificate.png' /></span>&nbsp;".(isset($emp['Profession']) && trim($emp['Profession']!='') ? $emp['Profession'] : 'Aucune profession')."</span>
                ".($emp['EMAIL']==null || $emp['EMAIL']=='' ? '' : "<br />
                <span class='userDataContacts' style='vertical-align:1px;background-color:#daf195;color:#5d6a36;border-color:#5d6a36;'><a class='standardButton noSpace tooltipS' title='Click to send an email' style='background-color:#e4a88b;font-size:11px;border-radius:3px;' href='mailto:{$emp['EMAIL']}'><img class='imgrectify4px' src='assets/imgs/mail.png' /></a>&nbsp;".($emp['EMAIL']==null || $emp['EMAIL']=='' ? 'Aucun email' : $emp['EMAIL'])."</span>")."

                <hr class='noSpace' style='margin-top:-5px;' />

                ".($emp['ADRESSE2']==null || $emp['ADRESSE2']=="" ? "" : "<span class='userDataContacts tooltipW' title='Adresse: {$emp['ADRESSE1']}, {$emp['ADRESSE2']}' style='background-color:#75a345;'><img style='margin:0;vertical-align:-3px;' src='assets/imgs/home.png' />&nbsp;".trim($emp['ADRESSE2'])."</span>")."

                ".($emp['TELHOME']==0 || $emp['TELHOME']==null || $emp['TELHOME']=="" ? "" : "<span class='userDataContacts tooltipW' title='Téléphone fixe: {$emp['TELHOME']}' style='background-color:#75a345;'><img style='margin:0;vertical-align:-3px;' src='assets/imgs/phone2.png' />&nbsp;".trim($emp['TELHOME'])."</span>")."

                ".($emp['TELWORK']==0 || $emp['TELWORK']==null || $emp['TELWORK']=="" ? "" : "<span class='userDataContacts tooltipW' title='Téléphone travail: {$emp['TELWORK']}' style='background-color:#f9afc5;'><img style='margin:0;vertical-align:-3px;' src='assets/imgs/phone.png' />&nbsp;".trim($emp['TELWORK'])."</span>")."

                ".($emp['TELMOB']==0 || $emp['TELMOB']==null || $emp['TELMOB']=="" ? "" : "<span class='userDataContacts tooltipW' title='Téléphone portable: {$emp['TELMOB']}' style='background-color:#75a345;'><img style='margin:0;vertical-align:-3px;' src='assets/imgs/phone2.png' />&nbsp;".trim($emp['TELMOB'])."</span>")."

            </div><span style='font-size:10px;font-weight:100;width:100%;float:left;'>
                    $employedSinceStr
                </span>
                ";
            echo "</div>
        </div>";
        };
    ?>
    <br><br><br>
    &nbsp;
    </div>