Git global setup

git config --global user.name "Sash Informatics (Presley)"

git config --global user.email "sashinformatics@zoho.com"

-------

Create a new repository

git clone git@gitlab.com:sashinformatics/zvzdb_public.git

cd crawler

touch README.md

git add README.md

git commit -m "add README"

git push -u origin master

-------

Existing folder

cd existing_folder

git init

git remote add origin git@gitlab.com:sashinformatics/zvzdb_public.git

git add .

git commit -m "Initial commit"

git push -u origin master

-------

Existing Git repository

cd existing_repo

git remote rename origin old-origin

git remote add origin git@gitlab.com:sashinformatics/zvzdb_public.git

git push -u origin --all

git push -u origin --tags

-------

IF MERGE PROBLEM

git pull origin master --allow-unrelated-histories

git merge origin origin/master

... add and commit here...

git push origin master

